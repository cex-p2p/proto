const express = require('express');
const protoLoader = require('@grpc/proto-loader');
const grpcLibrary = require('@grpc/grpc-js');
const app = express();

const PORT = 5555;

const protoPath = (path) => __dirname + '/../' + path;

const protoFiles = [
    protoPath('cex/bridge/bridge.proto'),
    protoPath('cex/bridge/coin.enum.proto'),
    protoPath('cex/bridge/network.enum.proto'),
];

const options = {};

const packageDefinition = protoLoader.loadSync(protoFiles, options);
const packageObject = grpcLibrary.loadPackageDefinition(packageDefinition);

console.dir(packageObject);

// app.use(express.static(__dirname /* path to index.html */));
// app.listen(PORT);

console.info(`Served at port ${PORT}`);
