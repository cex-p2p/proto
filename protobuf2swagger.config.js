module.exports = {
    files: [
        'cex/bridge/bridge.proto',
        'cex/bridge/network.enum.proto',
        'cex/bridge/coin.enum.proto',
    ],
    dist: 'swagger/apischema.json',
    long: 'number',
    customSchema: {
    },
};
